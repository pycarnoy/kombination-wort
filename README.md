KombinationWort-Regular is a font!

KombinationWort-Regular is an attempt to make words out of the 10 simple shapes of Josef Albers' KombinationSchrift.

The version 0.1 is ready to be downloaded at GitLab: https://gitlab.com/pycarnoy/kombination-wort.

The UFO is now available to all!

--pyc

![](images/nano-specimen-kwr-01.png)
